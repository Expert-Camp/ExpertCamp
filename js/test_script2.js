var data;
var ob;
var length=12;
var numberContentPerPage=8;
var job;

  $.ajax({
    url:'../json/people.json',
    type:'Get'
    })
    .done(function(response){  
        data = response.people;
        display(data, 8);
        // console.log(data);
       
    })
    .fail(function(){
    console.log("Error");
    });

    function display(obj,size){
      var str ="";
     var skill = "";
     var lang = "";
     var count =0;
     for(var i=0;i<size;i++){
             for(var j=0;j<obj[i].skill.length;j++){
                 skill += ` <li class="collection-item">`+obj[i].skill[j]+`</li>`;
             }
             for(var k=0;k<obj[i].language.length;k++){
                 lang += `<li class="collection-item">`+obj[i].language[k]+`</li>`;
             }
             str +=`<div class="col s12 m6 l3 gallery-item gallery-expand  gallery-filter" data-type="poly" data-section-type="product">
                        <div class="gallery-curve-wrapper">
                            <a class="gallery-cover gray">
                              <img
                                src="../image/`+obj[i].avarta+`"
                                alt="Sun"
                                crossorigin="anonymous"
                                data-product-featured-image style="width:100%" />
                            </a>
                            <a onclick=" M.toast({html: '`+obj[i].firstName+`', classes: 'rounded'});" class="btn-floating activator tooltipped  pulse btn-move-up waves-effect waves-light red accent-2 z-depth-4 right" data-position="bottom" data-delay="50" data-tooltip="More details">
                              <i class="material-icons">keyboard_arrow_up</i>
                            </a>
                            <div class="gallery-header">
                              <span class="title">`+obj[i].name+`</span><br>
                              <span class="title">`+obj[i].prefer+`</span>
                              <div class="spr-form-review-rating">
                                <label class="spr-form-label" for="review[rating]">Rating</label>
                                <div class="spr-form-input spr-starrating ">
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="1">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="2">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="3">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="4">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="5">&nbsp;</a>
                                </div>
                              </div>
                              
                            </div>
                          <div class="gallery-body">
                              <div class="title-wrapper">
                                <h3 class="card-title activator grey-text text-darken-4">`+obj[i].name+`</h3>
                              </div>
                              <div class="checkout-column">
                                      <div class="spr-form-review-rating">
                                      <label class="spr-form-label" for="review[rating]">Rating</label>
                                      <div class="spr-form-input spr-starrating ">
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="1">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="2">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="3">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="4">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="5">&nbsp;</a>
                                      </div>
                                    </div>
                                      <div class=" row">
                                          <a class="btn-large z-depth-0" >
                                          <span data-add-to-cart-text>
                                            Contact to me     
                                          </span>
                                        </a>
                                      </div >
                                      
                                  </form>
                              </div>
                              <div class="description">
                                    <p>
                                      <meta charset="utf-8"><span>
                                          <div class="card-reveal" >
                                            
                                            <p>Here is some more information about `+obj[i].name+` </p>
                                            <p>Country: <img style="width:50px" src="https://lipis.github.io/flag-icon-css/flags/4x3/`+obj[i].country+`.svg" ></p>
                                            <p>Gender: `+obj[i].gender+`</p>
                                            <p>Exerience: `+obj[i].experince+`</p>
                                            <p>
                                              <i class="material-icons">perm_identity</i> Skill:`+skill+`
                                            </p>
                                            <p>
                                              <i class="material-icons">school</i> Language:`+lang+`
                                            </p>
                                            <p>
                                                <i class="material-icons">perm_phone_msg</i> +855 `+obj[i].contact+`</p>
                                            <p>
                                              <i class="material-icons">email</i>`+obj[i].firstName+`@gmail.com</p>
                                            <p>
                                              <i class="material-icons">cake</i> `+obj[i].dob+`
                                            </p>
                                            <p>
                                              <i class="material-icons">airplanemode_active</i> 
                                            </p>
                                            <p>
                                                <h2 class="center"> Experience</h2>
                                                Developing cross-browser cross-platform Vanilla Javascript Web Component Library
                                                Built with everything I am current learning
                                                Built with my knowledge of the web over the past several years
                                            </p>
                                            </p>
                                                <h2 class="center"> Education</h2>
                                                <h5> Hack Reactor</h5>
                                                          2014
                                                Immersive JavaScript Engineering program
                                                Studied Javascript Engineering, Product Development, Application Deployment, and Market Validation.

                                                 <h5> College at SouthWestern </h5>
                                                 2010-2013
                                                 Studied for Bachelor in History of Ideas
                                                 Studied Humanities and ideas to structure and implement currently forming ideas.

                                                 <h5> University of Central Oklahoma</h5>
                                                 2009-2010
                                                Studied Engineering, Photography, Graphic Arts
                                                Studied a variety of professions, trying to find what I am able to exceed in.
                                            </p>
                                            </p>
                                          </div>
                                      
                                      </span>
                                    </p>
                                    <div id="shopify-product-reviews" data-id="`+i+`">
                                        <style scoped>
                                            .spr-  {
                                                padding: 24px;
                                                border-color: #ECECEC;
                                            }
                                            .spr-review, .spr-form {
                                                border-color: #ECECEC;
                                            }
                                        </style>
                                     
                                    </div>
                              </div>
                          </div>
                          <div class="gallery-action">
                              <a class="go-to-ratings btn-floating btn-large waves-effect waves-light">
                                <i class="material-icons">star</i>
                              </a>
                          </div>
                        </div>
                      </div>
                       `;
                 ++count;
                 if(count==4){
                     count=0;
                     str+="<div class='row'></div>";
                 }
                 skill="";
                 lang="";
            }  
            str +=`<!--[if (gt IE 9)|!(IE)]><!--><script src="../js/vendorfaa1.js" defer="defer"></script><!--<![endif]-->
                   <!--[if (gt IE 9)|!(IE)]><!--><script src="../js/themefaa1.js" defer="defer"></script><!--<![endif]-->`;
            $('#data').html(str);
    }

    //job part
    
    function createCardJob(image, companyName, jobTitle, modalId) {
      var card = `<div class="col s12 m3">
        <div class="card">
          <div class="card-image">
            <img width="200" height="180" src="` + image + `">
            <div class="fixed-action-btn direction-left">
              <a class="btn-floating btn-large red">
                <i class="large material-icons">share</i>
              </a>
            </div>
          </div>
          <div class="card-content">
          <h3 style="color: black;margin:0;padding 0" class="card-title">` + companyName + `</h3>
            <p style="color: black;white-space: nowrap; width: 150px;  overflow: hidden;text-overflow: ellipsis; ">` + jobTitle + `</p>
          </div>
          <div class="card-action">
            <a class="waves-effect modal-trigger" href="#` + modalId + `">Detail...</a>
          </div>
        </div>
      </div>`;
      return card;
    }
    
    function crateJobDescription(modalId, logo, term, year, func, hiring, industry, salary, qualification, sex, language, age, location, publisDate, closingDate, jobDescription, requirment) {
      var des = `<div id="` + modalId + `" class="modal modal-fixed-footer">
      <div class="modal-content grey lighten-5">
        <h5 style="color: black">
          <u> Job Description</u>
        </h5>
        <div class="row">
          <div class="s12 m12 l12 center">
            <img src="` + logo + `" alt="img-thumnail" width="200px" height="100px">
          </div>
          <div class="row">
            <div class="s12 m12 l12">
              <table class="spec-tbl" border="0">
                <tbody>
                  <tr>
                    <th scope="row">
                      <p>Level</p>
                    </th>
                    <td style="width:170px">
                      <p>Professional</p>
                    </td>
                    <th>
                      <p>Term</p>
                    </th>
                    <td>
                      <p>` + term + `</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <p>Year of Exp.</p>
                    </th>
                    <td>
                      <p>` + year + `</p>
                    </td>
                    <th>
                      <p>Function</p>
                    </th>
                    <td>
                      <p>` + func + `</p>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <p>Hiring </p>
                    </th>
                    <td>
                      <p>` + hiring + `</p>
                    </td>
                    <th scope="row">
                      <p>Industry</p>
                    </th>
                    <td>
                      <p>
                        ` + industry + `
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <p>Salary</p>
                    </th>
                    <td>
                      <p>` + salary + `</p>
                    </td>
                    <th scope="row">
                      <p>Qualification</p>
                    </th>
                    <td>
                      <p>
                       ` + qualification + `
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <p>Sex</p>
                    </th>
                    <td>
                      <p>
                        ` + sex + `
                      </p>
                    </td>
                    <th>
                      <p>Language</p>
                    </th>
                    <td>
                      <p>` + language + `</p>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <p>Age</p>
                    </th>
                    <td>
                      <p>` + age + `</p>
                    </td>
                    <th>
                      <p>Location</p>
                    </th>
                    <td>
                      <p>` + location + `</p>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">
                      <p>Publish Date</p>
                    </th>
                    <td>
                      <p>` + publisDate + `</p>
                    </td>
                    <th>
                      <p>Closing Date</p>
                    </th>
                    <td>
                      <p>` + closingDate + `</p>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="collection">
    
                <a href="#!" class="collection-item active">&gt;Job Description</a>
                <pre style="color: black">
                ` + jobDescription + `
                </pre>
    
              </div>
              <div class="collection">
    
                <a href="#!" class="collection-item active">&gt;Requirment</a>
                <pre style="color: black">
                ` + requirment + `
                </pre>
    
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
      </div>
    </div>`;
      return des;
    }
    
    function getDataJob() {
      var d;
      $.ajax({
        type: "GET",
        url: "../json/job.json",
        async: false
      }).done(function (data) {
        d = data;
      });
      return d;
    }
    
    function displayJob(pageNumner) {
      var startpos = (pageNumner * numberContentPerPage) - numberContentPerPage;
      var endPos = numberContentPerPage * pageNumner;
      var val;
      var row = $(`<div class="row"></div>`);
      $("#dataRows").empty();
      var count = 0;
      for (var i = startpos; i < endPos; i++) {
        if (job[i] != null) {
          val = job[i];
          row.append(createCardJob(val.logo, val.companyName, val.jobTitle, "mod" + (i)));
          $("#dataRows").append(row);
          $("#modals").append(crateJobDescription("mod" + i, val.logo, val.term, val.yearExp, val.function, val.hiring, val.industry, val.salary, val.qualification, val.sex, val.language, val.age, val.location, val.publisDate, val.closingDate, val.jobDescription, val.requirment));
          count++;
          if (count == 4) {
            count = 0;
            row = $(`<div class="row"></div>`);
          }
        }
      }
      $("#script-style").html(`<script src="../js/init.js"></script>`);
    }

    // finished job part
    $(document).ready(function() {
        job=getDataJob();
        displayJob(1);
       
  });
    
   