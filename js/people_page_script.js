var Shopify = Shopify || {};
Shopify.shop = "materialize-shopify-themes.myshopify.com";
Shopify.theme = {"name":"gallery-shopify-v1-0-0","id":184863757,"theme_store_id":null,"role":"main"};
Shopify.theme.handle = "null";
Shopify.theme.style = {"id":null,"handle":null};


(function() {
    function asyncLoad() {
      var urls = ["\/\/productreviews.shopifycdn.com\/assets\/v4\/spr.js"];
      for (var i = 0; i < urls.length; i++) {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = urls[i];
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
      }
    };
    if(window.attachEvent) {
      window.attachEvent('onload', asyncLoad);
    } else {
      window.addEventListener('load', asyncLoad, false);
    }
      })();

var data;
var ob;
var length=16;
  $.ajax({
    url:'../json/people.json',
    type:'Get',
    })
    .done(function(response){  
        data = response.people;
        display(data,length);
    })
    .fail(function(){
    console.log("Error");
    });

    function display(obj,size){
      var str ="";
     var skill = "";
     var lang = "";
     var count =0;
     for(var i=0;i<size;i++){
             for(var j=0;j<obj[i].skill.length;j++){
                 skill += ` <li class="collection-item">`+obj[i].skill[j]+`</li>`;
             }
             for(var k=0;k<obj[i].language.length;k++){
                 lang += `<li class="collection-item">`+obj[i].language[k]+`</li>`;
             }
             str +=`<div class="col s12 m6 l3 gallery-item gallery-expand  gallery-filter" data-type="poly" data-section-type="product">
                        <div class="gallery-curve-wrapper">
                            <a class="gallery-cover gray">
                              <img
                                src="../image/`+obj[i].avarta+`"
                                alt="Sun"
                                crossorigin="anonymous"
                                data-product-featured-image style="width:100%" />
                            </a>
                            <a onclick=" M.toast({html: '`+obj[i].firstName+`', classes: 'rounded'});" class="btn-floating activator tooltipped  pulse btn-move-up waves-effect waves-light red accent-2 z-depth-4 right" data-position="bottom" data-delay="50" data-tooltip="More details">
                              <i class="material-icons">keyboard_arrow_up</i>
                            </a>
                            <div class="gallery-header">
                              <span class="title">`+obj[i].name+`</span><br>
                              <span class="title">`+obj[i].prefer+`</span>
                              <div class="spr-form-review-rating">
                                <label class="spr-form-label" for="review[rating]">Rating</label>
                                <div class="spr-form-input spr-starrating ">
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="1">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="2">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="3">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="4">&nbsp;</a>
                                  <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="5">&nbsp;</a>
                                </div>
                              </div>
                              
                            </div>
                          <div class="gallery-body">
                              <div class="title-wrapper">
                                <h3 class="card-title activator grey-text text-darken-4">`+obj[i].name+`</h3>
                              </div>
                              <div class="checkout-column">
                                      <div class="spr-form-review-rating">
                                      <label class="spr-form-label" for="review[rating]">Rating</label>
                                      <div class="spr-form-input spr-starrating ">
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="1">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="2">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="3">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star" data-value="4">&nbsp;</a>
                                        <a href="#" onclick="SPR.setRating(this);return false;" class="spr-icon spr-icon-star spr-icon-star-empty" data-value="5">&nbsp;</a>
                                      </div>
                                    </div>
                                      <div class=" row">
                                          <a class="btn-large z-depth-0" >
                                          <span data-add-to-cart-text>
                                            Contact to me     
                                          </span>
                                        </a>
                                      </div >
                                      
                                  </form>
                              </div>
                              <div class="description">
                                    <p>
                                      <meta charset="utf-8"><span>
                                          <div class="card-reveal" >
                                            
                                            <p>Here is some more information about `+obj[i].name+` </p>
                                            <p>Country: <img style="width:50px" src="https://lipis.github.io/flag-icon-css/flags/4x3/`+obj[i].country+`.svg" ></p>
                                            <p>Gender: `+obj[i].gender+`</p>
                                            <p>Exerience: `+obj[i].experince+`</p>
                                            <p>
                                              <i class="material-icons">perm_identity</i> Skill:`+skill+`
                                            </p>
                                            <p>
                                              <i class="material-icons">school</i> Language:`+lang+`
                                            </p>
                                            <p>
                                                <i class="material-icons">perm_phone_msg</i> +855 `+obj[i].contact+`</p>
                                            <p>
                                              <i class="material-icons">email</i>`+obj[i].firstName+`@gmail.com</p>
                                            <p>
                                              <i class="material-icons">cake</i> `+obj[i].dob+`
                                            </p>
                                            <p>
                                              <i class="material-icons">airplanemode_active</i> 
                                            </p>
                                            <p>
                                                <h2 class="center"> Experience</h2>
                                                Developing cross-browser cross-platform Vanilla Javascript Web Component Library
                                                Built with everything I am current learning
                                                Built with my knowledge of the web over the past several years
                                            </p>
                                            </p>
                                                <h2 class="center"> Education</h2>
                                                <h5> Hack Reactor</h5>
                                                          2014
                                                Immersive JavaScript Engineering program
                                                Studied Javascript Engineering, Product Development, Application Deployment, and Market Validation.

                                                 <h5> College at SouthWestern </h5>
                                                 2010-2013
                                                 Studied for Bachelor in History of Ideas
                                                 Studied Humanities and ideas to structure and implement currently forming ideas.

                                                 <h5> University of Central Oklahoma</h5>
                                                 2009-2010
                                                Studied Engineering, Photography, Graphic Arts
                                                Studied a variety of professions, trying to find what I am able to exceed in.
                                            </p>
                                            </p>
                                          </div>
                                      
                                      </span>
                                    </p>
                                    <div id="shopify-product-reviews" data-id="`+i+`">
                                        <style scoped>
                                            .spr-container {
                                                padding: 24px;
                                                border-color: #ECECEC;
                                            }
                                            .spr-review, .spr-form {
                                                border-color: #ECECEC;
                                            }
                                        </style>
                                     
                                    </div>
                              </div>
                          </div>
                          <div class="gallery-action">
                              <a class="go-to-ratings btn-floating btn-large waves-effect waves-light">
                                <i class="material-icons">star</i>
                              </a>
                          </div>
                        </div>
                      </div>
                       `;
                 ++count;
                 if(count==4){
                     count=0;
                     str+="<div class='row'></div>";
                 }
                 skill="";
                 lang="";
            }  
            str +=`<script src="../js/vendorfaa1.js" defer="defer"></script>
                   <script src="../js/themefaa1.js" defer="defer"></script>`;
            $('#data').html(str);
            var fiter =`  <div class=" center container">
                            <ul class="collection with-header white">
                                <li class="collection-header active " style="background-color:whitesmoke"><h4>Search Filter</h4></li>
                            </ul>
                         </div>
                         <div class="container">
                            <div class="row">
                              <!-- start country fiter content -->
                              <div class="input-field col s12 m12 l5 offset-l1">
                                <select id="country">
                                  <option value="" disabled selected>Choose your option</option>
                                  <option value="ca">Canada</option>
                                  <option value="kh">Cambodia</option>
                                  <option value="cn">China</option>
                                  <option value="kr">Korean</option>
                                  <option value="th">Thailand</option>
                                  <option value="in">India</option>
                                  <option value="la">Lao</option>
                                </select>
                                <label>Country</label>
                              </div>
                              <!-- end country fiter content -->

                              
                              <!-- start job fiter content -->
                              <div class="input-field col s12 m12 l5 ">
                                  <select id="job">
                                    <option value="" disabled selected>Choose your option</option>
                                    <option value="C#">C#</option>
                                    <option value="Java">Java</option>
                                    <option value="Web">Web</option>
                                    <option value="Php">PHP</option>
                                    <option value="Spring">Spring</option>
                                    <option value="IOS">IOS</option>
                                    <option value="Android">Android</option>
                                  </select>
                                  <label>Job Title</label>
                              </div>
                              <!-- end job filter content -->
                            </div>
                          </div>
                        </div>`;
                        $('#filte').html(fiter);

    }
    function searchCountry(obj, value, job){
        var result = new Array();
        var str ="";
        var skill = "";
        var language = "";
        var count = 0;
        if(obj!=null){
           for(var i=0;i<obj.length;i++){
              for(var j=0;j<obj[i].skill.length;j++){
                 if(obj[i].country==value && obj[i].skill[j] == job){
                      result.push(data[i]);

                 }
                }
           }
           display(result,result.length);
           if(result.length<1){
           
             swal("Not Found !!!");
           }
        }else{
          swal("Please select your option first !");
        }
   
    }
    $(document).ready(function() {
       
        $("#filter").click(function(){
            searchCountry(data,$("#country").val(),$("#job").val());
        });
      
  });
 