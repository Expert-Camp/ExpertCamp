(function ($) {
  $(function () {

    $('.sidenav').sidenav();
     $('.parallax').parallax();

  });
})(jQuery);
$(document).ready(function () {
  $('.modal').modal();
});
$(document).ready(function () {
  $(".search").hide();
  $("#searchs").click(function () {
    $(".search").slideToggle(1000);
  });
  $(".email-signup").hide();
  $("#signup-box-link").click(function () {
    $(".email-login").fadeOut(500);
    $(".email-signup").delay(500).fadeIn(500);
    $("#login-box-link").removeClass("active");
    $("#signup-box-link").addClass("active");
  });
  $("#login-box-link").click(function () {
    $(".email-login").delay(500).fadeIn(500);
    $(".email-signup").fadeOut(500);
    $("#login-box-link").addClass("active");
    $("#signup-box-link").removeClass("active");
  });
});
document.addEventListener('DOMContentLoaded', function () {
  var elems = document.querySelectorAll('.fixed-action-btn');
  var instances = M.FloatingActionButton.init(elems, {
    direction: 'left'
  });
});
document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.datepicker');
  var instances = M.Datepicker.init(elems, options);
});

// Or with jQuery

$(document).ready(function(){
  $('.datepicker').datepicker();
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.chips');
  var instances = M.Chips.init(elems, options);
});

// Or with jQuery

$('.chips').chips();
$('.chips-initial').chips({
  data: [{
    tag: 'Apple',
  }, {
    tag: 'Microsoft',
  }, {
    tag: 'Google',
  }],
});
$('.chips-placeholder').chips({
  placeholder: 'Enter a tag',
  secondaryPlaceholder: '+Tag',
});
$('.chips-autocomplete').chips({
  autocompleteOptions: {
    data: {
      'Apple': null,
      'Microsoft': null,
      'Google': null
    },
    limit: Infinity,
    minLength: 1
  }
});

var chip = {
  tag: 'chip content',
  image: '', //optional
};