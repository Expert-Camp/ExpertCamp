var job;
var numberContentPerPage = 6;
var tempValue;
$(document).ready(function () {
  $('select').formSelect();
  job = getData();
  generatePagination(job);
  display(1);
  $("#search-button").click(search);
  $("#btnFilter").click(filter);
});

function createCard(image, companyName, jobTitle, modalId) {
  var card = `<div class="col s12 m6 l4">
    <div class="card">
      <div class="card-image">
        <img width="100%" height="180px" src="` + image + `">
        <div class="fixed-action-btn direction-left">
          <a class="btn-floating btn-large red">
            <i class="large material-icons">share</i>
          </a>
        </div>
      </div>
      <div class="card-content">
      <h3 style="color: black;margin:0;padding 0" class="card-title">` + companyName + `</h3>
        <p style="color: black;white-space: nowrap; width: 200px; overflow: hidden;text-overflow: ellipsis; "><b>` + jobTitle + `</b></p>
      </div>
      <div class="card-action">
        <a class="waves-effect modal-trigger" href="#` + modalId + `">Detail...</a>
      </div>
    </div>
  </div>`;
  return card;
}

function crateJobDescription(modalId, logo, term, year, func, hiring, industry, salary, qualification, sex, language, age, location, publisDate, closingDate, jobDescription, requirment) {
  var des = `<div id="` + modalId + `" class="modal modal-fixed-footer">
  <div class="modal-content grey lighten-5">
    <h5 style="color: black">
      <u> Job Description</u>
    </h5>
    <div class="row">
      <div class="s12 m12 l12 center">
        <img src="` + logo + `" alt="img-thumnail" width="200px" height="100px">
      </div>
      <div class="row">
        <div class="s12 m12 l12">
          <table class="spec-tbl" border="0">
            <tbody>
              <tr>
                <th scope="row">
                  <p>Level</p>
                </th>
                <td style="width:170px">
                  <p>Professional</p>
                </td>
                <th>
                  <p>Term</p>
                </th>
                <td>
                  <p>` + term + `</p>
                </td>
              </tr>
              <tr>
                <th>
                  <p>Year of Exp.</p>
                </th>
                <td>
                  <p>` + year + `</p>
                </td>
                <th>
                  <p>Function</p>
                </th>
                <td>
                  <p>` + func + `</p>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <p>Hiring </p>
                </th>
                <td>
                  <p>` + hiring + `</p>
                </td>
                <th scope="row">
                  <p>Industry</p>
                </th>
                <td>
                  <p>
                    ` + industry + `
                  </p>
                </td>
              </tr>
              <tr>
                <th>
                  <p>Salary</p>
                </th>
                <td>
                  <p>` + salary + `</p>
                </td>
                <th scope="row">
                  <p>Qualification</p>
                </th>
                <td>
                  <p>
                   ` + qualification + `
                  </p>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <p>Sex</p>
                </th>
                <td>
                  <p>
                    ` + sex + `
                  </p>
                </td>
                <th>
                  <p>Language</p>
                </th>
                <td>
                  <p>` + language + `</p>
                </td>
              </tr>
              <tr>
                <th>
                  <p>Age</p>
                </th>
                <td>
                  <p>` + age + `</p>
                </td>
                <th>
                  <p>Location</p>
                </th>
                <td>
                  <p>` + location + `</p>
                </td>
              </tr>
              <tr>
                <th scope="row">
                  <p>Publish Date</p>
                </th>
                <td>
                  <p>` + publisDate + `</p>
                </td>
                <th>
                  <p>Closing Date</p>
                </th>
                <td>
                  <p>` + closingDate + `</p>
                </td>
              </tr>
            </tbody>
          </table>
          <div class="collection">

            <a href="#!" class="collection-item active">&gt;Job Description</a>
            <pre style="color: black">
            ` + jobDescription + `
            </pre>

          </div>
          <div class="collection">

            <a href="#!" class="collection-item active">&gt;Requirment</a>
            <pre style="color: black">
            ` + requirment + `
            </pre>

          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-green btn-flat">close</a>
  </div>
</div>`;
  return des;
}

function getData() {
  var d;
  $.ajax({
    type: "GET",
    url: "../json/job.json",
    async: false
  }).done(function (data) {
    d = data;
  });
  return d;
}

function display(pageNumner) {
  var startpos = (pageNumner * numberContentPerPage) - numberContentPerPage;
  var endPos = numberContentPerPage * pageNumner;
  var val;
  var row = $(`<div class="row"></div>`);
  $("#dataRows").empty();
  var count = 0;
  for (var i = startpos; i < endPos; i++) {
    if (job[i] != null) {
      val = job[i];
      row.append(createCard(val.logo, val.companyName, val.jobTitle, "mod" + (i)));
      $("#dataRows").append(row);
      $("#modals").append(crateJobDescription("mod" + i, val.logo, val.term, val.yearExp, val.function, val.hiring, val.industry, val.salary, val.qualification, val.sex, val.language, val.age, val.location, val.publisDate, val.closingDate, val.jobDescription, val.requirment));
      count++;
      if (count == 3) {
        count = 0;
        row = $(`<div class="row"></div>`);
      }
    }
  }
  $("#script-style").html(`<script src="../js/init.js"></script>`);
}

function search() {
  $("#dataRows").empty();
  var tmp = new Array();
  $.ajax({
    type: "GET",
    url: "../json/job.json",
    async: false
  }).done(function (data) {
    var title = $("#searchText").val();
    var isFound = false;
    $.each(data, function (key, val) {
      if (val.jobTitle.toLowerCase().includes(title.toLowerCase())) {
        tmp.push(val);
        isFound = true;
      }
    });
    job = tmp;
    generatePagination(tmp);
    if (!isFound) {
      $("#dataRows").append(`<h1 style="color:red">No data was found!!!.</h1>`);
    } else {
      display(1);
    }
  });
}

function filter() {
  var location = $("#location").val();
  var typeJob = $("#typeJob").val();
  var tmp = new Array();
  var isFound = false;
  if (location != null && typeJob != null) {
    $("#dataRows").empty();
    $.ajax({
      type: "GET",
      url: "../json/job.json",
      async: false
    }).done(function (data) {
      $.each(data, function (key, val) {
        if ((val.function.toLowerCase().includes(typeJob.toLowerCase())) && (val.location.toLowerCase().includes(location.toLowerCase()))) {
          tmp.push(val);
          isFound = true;
        }
      });
      job = tmp;
      generatePagination(tmp);
      if (isFound) {
        display(1);
      } else {
        $("#dataRows").append(`<h1 style="color:red">No data was found!!!.</h1>`);
      }
    });
  }
}

function generatePagination(job) {
  var remainpage = job.length % numberContentPerPage;
  if (remainpage != 0) {
    remainpage = 1;
  }
  var totalpage = remainpage + Math.floor((job.length / numberContentPerPage));
  $("#pages").empty();
  var firstPage = `
  <li value="` + job + `"  page-value="1" class="active waves-effect" onClick="pageNumClick(this)">
    <a href="#!">1</a>
  </li>`;
  $("#pages").append(firstPage);
  for (var i = 1; i < totalpage; i++) {
    var page = ` 
    <li page-value="` + (i + 1) + `" class="waves-effect" onClick="pageNumClick(this)">
      <a href="#!">` + (i + 1) + `</a>
    </li>`
    $("#pages").append(page);
  }
}

function pageNumClick(ele) {
  $("#pages").children().removeClass("active");
  $(ele).addClass("active");
  display($(ele).attr("page-value"));
}