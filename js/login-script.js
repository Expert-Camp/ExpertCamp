var users=new Array();
var emailValidator=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
$(document).ready(function () {
    loadData();
    $("#btnlogin").click(login);
    $("#btnsignup").click(signUp);
});
function loadData()
{
    $.getJSON( "../json/users.json", function( data ) {
        $.each( data, function( key, val ) {
          users.push(new User(val.firstName,val.lastName,val.email,val.password));
        });
    });
}
function refresh()
{
    resetMessage();
}
function resetMessage()
{
    // log in
    $("#errEmail").hide();
    $("#errId").hide();
    $("#errlogin").hide();
    // sign up
    $("#errFirstName").hide();
    $("#errLastName").hide();
    $("#errEmail_S").hide();
    $("#errPassword_S").hide();
    $("#errCPassword_S").hide();
    
}
function isNull(values,lblerrors)
{
    var isnull=false;
    for(var i=0;i<values.length;i++)
    {
        var c=values[i].trim();
        if(c=='')
        {
            $(lblerrors[i]).text("Value can't be null or empty!!")
            $(lblerrors[i]).show();
            isnull=true;
        }
    }
    return isnull;
}

function User(firstName, lastName, email, password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
}

function isCorrectUser(email, password) {
    var isFound = false;
    for (var i = 0; i < users.length; i++) {
        var user = users[i];
        if (user.email == email && user.password == password) {
            isFound = true;
            return true;
        }
    }
    if (!isFound) {
        return false;
    }
}

function isDuplicateEmail(email)
{
    var isFound = false;
    for (var i = 0; i < users.length; i++) {
        var user = users[i];
        if (user.email.trim() == email.trim()) {
            isFound = true;
            break;
        }
    }
    return isFound;
}
// login
function login() {

    resetMessage();
    var email = $("#txtemail").val();
    var password = $("#txtpassword").val();
    var cons=new Array();
    cons.push(email);
    cons.push(password);

    var errors=new Array();
    errors.push("#errEmail");
    errors.push("#errId");
    if(!isNull(cons,errors))
    {
        if(!isCorrectUser(email,password))
        {
            $("#errlogin").show();
        }
        else
        {
            window.open("../index.html","_self");
        }
    }
}

// sign up

function signUp() {

    resetMessage();
    var firstName=$("#txtfirstname_s").val().trim();
    var lastName=$("#txtlastname_s").val().trim();
    var email = $("#txtemail_s").val().trim();
    var password = $("#txtpassword_s").val().trim();
    var cpassword=$("#txtcpassword_s").val().trim();
    var cons=new Array();
    // values
    cons.push(firstName);
    cons.push(lastName);
    cons.push(email);
    cons.push(password);
    cons.push(cpassword);

    var errors=new Array();
    // label error
    errors.push("#errFirstName");
    errors.push("#errLastName");
    errors.push("#errEmail_S");
    errors.push("#errPassword_S");
    errors.push("#errCPassword_S");

    if(!isNull(cons,errors))
    {

        if(!emailValidator.test(email))
        {
            var e=$("#errEmail_S");
            e.text("Email is not valid.")
            e.show();
        }
        else if(isDuplicateEmail(email))
        {
            var e=$("#errEmail_S");
            e.text("The email is already existed.")
            e.show();
        }
        else if(password!=cpassword)
        {
            var p=$("#errCPassword_S");
            p.text("Confirm password and Password do not match.")
            p.show();
        }
        else
        {
            // do somthing when signup success

        }

    }
}


