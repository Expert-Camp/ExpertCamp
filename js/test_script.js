var Shopify = Shopify || {};
Shopify.shop = "materialize-shopify-themes.myshopify.com";
Shopify.theme = {"name":"gallery-shopify-v1-0-0","id":184863757,"theme_store_id":null,"role":"main"};
Shopify.theme.handle = "null";
Shopify.theme.style = {"id":null,"handle":null};


(function() {
    function asyncLoad() {
      var urls = ["\/\/productreviews.shopifycdn.com\/assets\/v4\/spr.js"];
      for (var i = 0; i < urls.length; i++) {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = urls[i];
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
      }
    };
    if(window.attachEvent) {
      window.attachEvent('onload', asyncLoad);
    } else {
      window.addEventListener('load', asyncLoad, false);
    }
      })();